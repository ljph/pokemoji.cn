// $(function(){
// 	$(window).scroll(function(e){
// 		$(window).scrollTop()>0?$(".wrapper").addClass("small-header"):$(".wrapper").removeClass("small-header")}
// })
$(function(){
	var all = $(window).height(),
		html = $("html").height(),
		head = $("nav")[0].offsetHeight,
		footer = ($("footer")[0].offsetHeight >0)?$("footer")[0].offsetHeight : $(".footer_app")[0].offsetHeight;
	if(html < all){
		$(".main").height(all-head-footer-1);
	}	
	$(window).scroll(function(e){
		var t = document.documentElement.scrollTop || document.body.scrollTop;
		if($(".produre").length > 0){
			if(t >= $(".produre")[0].offsetTop-100){
				$(".introduce").find(".col-md-6").eq(0).addClass("animated fadeInLeft");
				$(".introduce").find(".col-md-6").eq(1).addClass("animated fadeInRight");
			}
		}
	})
	$("#fit").change(function(){
		var value = parseInt($(this).val());
	  	switch(value)
		{
			case 0:
			  $(".fee").html('500');
			  break;
			case 1:
			  $(".fee").html('800');
			  break;
			case 2:
			  $(".fee").html('1500');
			  break;
			default:
			  $(".fee").html('2000');
		}
	});
	// $(".tab li").click(function(){
	// 	var index = $(this).index();
	// 	$(this).addClass("on").siblings("li").removeClass("on");
	// 	$(".method1").eq(index).show().siblings(".method1").hide()
	// })
})