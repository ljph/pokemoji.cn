import Vue from 'vue'
import Router from 'vue-router'
import Header from 'components/common/header'
import Footer from 'components/common/footer'
import Index from 'components/index/index'
import Contact from 'components/contact/contact'
import QyCustomize from 'components/qyCustomize/qyCustomize'
import OrderWxapp from 'components/orderWxapp/orderWxapp'
import ShootTips from 'components/shootTips/shootTips'
import StoreAddr from 'components/storeAddr/storeAddr'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/index'
    },
    {
      path: '/index',
      name: 'index',
      components: {
        header: Header,
        mainPage: Index,
        footer: Footer
      }
    },
    {
      path: '/contact',
      name: 'contact',
      components: {
        header: Header,
        mainPage: Contact,
        footer: Footer
      }
    },
    {
      path: '/qyCustomize',
      name: 'qyCustomize',
      components: {
        header: Header,
        mainPage: QyCustomize,
        footer: Footer
      }
    },
    {
      path: '/orderWxapp',
      name: 'orderWxapp',
      components: {
        header: Header,
        mainPage: OrderWxapp,
        footer: Footer
      }
    },
    {
      path: '/shootTips',
      name: 'shootTips',
      components: {
        header: Header,
        mainPage: ShootTips,
        footer: Footer
      }
    },
    {
      path: '/storeAddr',
      name: 'storeAddr',
      components: {
        header: Header,
        mainPage: StoreAddr,
        footer: Footer
      }
    }
  ]
})
